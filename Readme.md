# Artnet-Serial

Listens for Art-Net udp packets and transfers the via serial interface(s) to ESP8266s running [serial-ws2812b](https://gitlab.com/pentagonum/serial-ws2812b) or similar. 

The ESP8266s always tell the Art-Net universe they want to subscribe to.
