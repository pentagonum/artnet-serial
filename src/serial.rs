use crate::DATA;
use crossbeam_channel::bounded;
use glob::glob;
use lazy_static::lazy_static;
use log::{debug, error, info, trace};
use serial::prelude::*;
use std::{
    collections::HashSet,
    io::prelude::*,
    path::PathBuf,
    sync::Mutex,
    thread,
    time::{Duration, Instant},
};

lazy_static! {
    static ref OPEN_PORTS: Mutex<HashSet<PathBuf>> = Mutex::new(HashSet::new());
}

pub fn open_ports() {
    glob("/dev/ttyUSB*")
        .expect("Failed to match glob pattern")
        .filter_map(std::result::Result::ok)
        .for_each(open_port);
}

fn stop(device: &PathBuf, error: String) {
    error!("Stopping because of error: {}", error);
    thread::sleep(Duration::from_secs(3)); // give it 3 seconds to retry a port
    {
        let mut open_ports = OPEN_PORTS.lock().unwrap();
        open_ports.remove(device);
    }
    panic!("Stopped because of error: {}", error);
}

fn open_port(device: PathBuf) {
    let should_open = {
        let mut open_ports = OPEN_PORTS.lock().unwrap();
        open_ports.insert(device.clone())
    };
    if !should_open {
        return;
    }

    info!("Opening port {:?}", device);
    if let Err(e) = thread::Builder::new()
        .name(format!("serial:{:?}", device))
        .spawn(move || {
            let mut port = serial::open(&device).unwrap_or_else(|e| {
                stop(
                    &device,
                    format!("Could open serial port {:?}: {:}", device, e),
                );
                panic!();
            });
            port.reconfigure(&|settings| {
                settings
                    .set_baud_rate(serial::BaudOther(460_800))
                    .unwrap_or_else(|e| {
                        stop(
                            &device,
                            format!("Could not set baudrate on {:?}: {:}", device, e),
                        );
                    });
                settings.set_char_size(serial::Bits8);
                settings.set_parity(serial::ParityNone);
                settings.set_stop_bits(serial::Stop1);
                settings.set_flow_control(serial::FlowNone);
                Ok(())
            })
            .unwrap_or_else(|e| {
                stop(
                    &device,
                    format!("Could not reconfigure {:?}: {:}", device, e),
                )
            });
            port.set_timeout(Duration::from_secs(10))
                .unwrap_or_else(|e| {
                    stop(
                        &device,
                        format!("Could not set timeout on {:?}: {:}", device, e),
                    );
                });

            info!("Opened port {:?}", device);
            thread::sleep(Duration::from_secs(1));
            info!("Asking for id on {:?}", device);
            {
                let buf = vec![0; 300];
                port.write_all(&buf).unwrap_or_else(|e| {
                    stop(
                        &device,
                        format!("Could not ask for id on {:?}: {:}", device, e),
                    );
                });
            }
            let mut read_buf: Vec<u8> = vec![0; 1000];

            let mut start = Instant::now();
            let mut frames = 0;
            let (tx, rx) = bounded(0);
            let mut sender = Some(tx);
            loop {
                match port.read(&mut read_buf) {
                    Ok(count) => {
                        if count == 0 {
                            thread::sleep(Duration::from_millis(1));
                            continue;
                        }

                        let id: u16 = read_buf[0].into();
                        debug!("Received id on {:?}: {}", device, id);
                        if let Some(tx) = sender.take() {
                            info!("Registering for artnet data: {:?}/{}", device, id);
                            let mut data = DATA.write().expect("Could not get write lock for DATA");
                            data.insert(id, tx);
                        }

                        let data = rx.recv().unwrap_or_else(|_| {
                            stop(
                                &device,
                                format!("Could not receive data on {:?}/{}", device, id),
                            );
                            panic!();
                        });
                        trace!(
                            "Sending data (length: {}) on {:?}/{}: {:?}",
                            data.len(),
                            device,
                            id,
                            data
                        );
                        port.write_all(&data).unwrap_or_else(|_| {
                            stop(
                                &device,
                                format!("Could not write data on {:?}/{}", device, id),
                            );
                        });

                        frames += 1;
                        if start.elapsed().as_secs() >= 1 {
                            debug!("{} fps on {:?}/{}", frames, device, id);
                            start = Instant::now();
                            frames = 0;
                        }
                    }
                    Err(e) => {
                        stop(&device, format!("Could not read from port: {:?}", e));
                    }
                }
            }
        })
    {
        panic!("Could not spawn serial thread: {:?}", e);
    }
}
