mod artnet;
mod serial;

use crossbeam_channel::Sender;
use fern::colors::{Color, ColoredLevelConfig};
use lazy_static::lazy_static;
use std::{
    collections::HashMap,
    sync::RwLock,
    time::Duration,
    {io, thread},
};

lazy_static! {
    static ref DATA: RwLock<HashMap<u16, Sender<Vec<u8>>>> = RwLock::new(HashMap::new());
}

fn init_logging() {
    let mut base_config = fern::Dispatch::new();
    let level = std::env::var("RUST_LOG")
        .unwrap_or_else(|_| "".to_owned())
        .to_lowercase();
    base_config = match level.as_ref() {
        "trace" => base_config.level(log::LevelFilter::Trace),
        "debug" => base_config.level(log::LevelFilter::Debug),
        "info" => base_config.level(log::LevelFilter::Info),
        "warn" => base_config.level(log::LevelFilter::Warn),
        _ => base_config.level(log::LevelFilter::Info),
    };

    let colors_level = ColoredLevelConfig::new()
        .error(Color::Red)
        .warn(Color::Yellow)
        .info(Color::Green);
    let stdout_config = fern::Dispatch::new()
        .format(move |out, message, record| {
            out.finish(format_args!(
                "{}: {} - {}",
                colors_level.color(record.level()),
                record.target(),
                message
            ))
        })
        .chain(io::stdout());

    base_config
        .chain(stdout_config)
        .apply()
        .expect("Could not initialize logger");
}

fn main() {
    init_logging();
    log_panics::init();
    artnet::init();
    loop {
        serial::open_ports();
        thread::sleep(Duration::from_secs(1));
    }
}
