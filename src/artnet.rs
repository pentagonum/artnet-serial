use crate::DATA;
use artnet_protocol::ArtCommand;
use log::{debug, info, trace};
use std::{
    net::{IpAddr, Ipv4Addr, UdpSocket},
    thread,
    time::{Duration, Instant},
};

fn ip(name: String) -> Result<Ipv4Addr, String> {
    get_if_addrs::get_if_addrs()
        .map_err(|e| format!("Error getting interface addresses: {:?}", e))
        .and_then(|mut addrs| {
            addrs
                .drain(..)
                .filter(|iface| iface.name == name)
                .filter_map(|iface| match iface.addr.ip() {
                    IpAddr::V4(ip) => Some(ip),
                    _ => None,
                })
                .next()
                .ok_or_else(|| "Could not find ip address".to_owned())
        })
}

pub fn init() {
    let ip_address: Ipv4Addr = ip("eth0".to_owned())
        .or_else(|_| ip("wlp3s0".to_owned()))
        .expect("Could not find ip adress");
    info!("Ip address: {}", ip_address);

    if let Err(e) = thread::Builder::new()
        .name("artnet".to_owned())
        .spawn(move || {
            let socket =
                UdpSocket::bind(("0.0.0.0", 6454)).expect("Could not bind socket for artnet");
            match socket.set_nonblocking(true) {
                Ok(_) => info!("Activated non-blocking mode"),
                Err(e) => info!("Could not activate non-blocking mode: {}", e),
            };
            info!("Bound socket for artnet");
            let mut start = Instant::now();
            let mut frames = 0;
            loop {
                let mut buffer = [0u8; 1024];
                if let Ok((length, _addr)) = socket.recv_from(&mut buffer) {
                    let command = ArtCommand::from_buffer(&buffer[..length])
                        .expect("Could not parse artnet command");

                    trace!("Received artnet data: {:?}", command);
                    if let ArtCommand::Output(output) = command {
                        frames += 1;
                        if start.elapsed().as_secs() >= 1 {
                            debug!("{} fps", frames);
                            start = Instant::now();
                            frames = 0;
                        }
                        let data = DATA.read().expect("Could not get read lock for DATA");
                        if let Some(sender) = data.get(&output.subnet) {
                            let mut data = output.data.clone();
                            data.truncate(300);
                            let _ = sender.try_send(data);
                        }
                    }
                } else {
                    thread::sleep(Duration::from_millis(1));
                }
            }
        })
    {
        panic!("Could not spawn artnet thread: {:?}", e);
    }
}
